import 'package:flutter/material.dart';
import '../pages/calls_page.dart';
import '../pages/camera_page.dart';
import '../pages/chat_page.dart';
import '../pages/status_page.dart';

class TabBarViewWidget extends StatelessWidget {
  const TabBarViewWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const TabBarView(
      children: [
        CameraPage(),
        ChatPage(),
        StatusPage(),
        CallsPage(),
      ],
    );
  }
}
