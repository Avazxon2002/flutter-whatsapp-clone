import 'package:flutter/material.dart';

class TabBarWidget extends StatelessWidget {
  const TabBarWidget({Key? key,}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const TabBar(
      tabs: [
        Tab(
          icon: Icon(
            Icons.camera_alt,
            color: Colors.white,
          ),
        ),
        Tab(
          text: 'CHATS',
        ),
        Tab(
          text: 'STATUS',
        ),
        Tab(
          text: 'CALLS',
        ),
      ],
    );
  }
}
