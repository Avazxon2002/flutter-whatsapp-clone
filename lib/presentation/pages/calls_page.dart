import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/components/dialog.dart';
import 'package:whatsapp_clone/presentation/models/call_model.dart';
import 'package:whatsapp_clone/presentation/widgets/theme/style.dart';

class CallsPage extends StatelessWidget {
  const CallsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
        itemCount: callData.length,
        itemBuilder: (context, i) {
          return Container(
            margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        //for image
                        SizedBox(
                          height: 55,
                          width: 55,
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(25)),
                            child: Image.asset(callData[i].avatar),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        // chats
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              callData[i].name,
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Row(
                              children: [
                                Container(
                                  child: callData[i].callType,
                                ),
                                Text(
                                  callData[i].time,
                                  maxLines: 1,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ],
                            ),
                          ],
                        )
                      ],
                    ),
                    InkWell(
                        onTap: () => showDialog(
                              context: context,
                              builder: (BuildContext context) => const DialogComponent(),
                            ),
                        child: const Icon(
                          Icons.call,
                          color: primaryColor,
                        ))
                  ],
                ),
                const Padding(
                  padding: EdgeInsets.only(
                    left: 60,
                    right: 10,
                  ),
                  child: Divider(
                    thickness: 1.50,
                  ),
                ),
              ],
            ),
          );
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        backgroundColor: primaryColor,
        child: const Icon(
          Icons.add_call,
          color: Colors.white,
        ),
      ),
    );
  }
}
