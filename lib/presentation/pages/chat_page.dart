import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/Models/chat_model.dart';
import 'package:whatsapp_clone/presentation/widgets/theme/style.dart';

class ChatPage extends StatelessWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Expanded(
            child: ListView.builder(
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: chatData.length,
              itemBuilder: (context, i) {
                return Container(
                  margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              //for image
                              SizedBox(
                                height: 55,
                                width: 55,
                                child: ClipRRect(
                                  borderRadius: const BorderRadius.all(
                                      Radius.circular(25)),
                                  child: Image.asset(
                                      chatData[i].avatar,
                                  ),
                                ),
                              ),
                              const SizedBox(
                                width: 10,
                              ),
                              // chats
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    chatData[i].name,
                                    style: const TextStyle(
                                      fontSize: 18,
                                      fontWeight: FontWeight.w500,
                                    ),
                                  ),
                                  const SizedBox(
                                    height: 5,
                                  ),
                                  Text(
                                    chatData[i].message,
                                    maxLines: 1,
                                    overflow: TextOverflow.ellipsis,
                                  ),
                                ],
                              )
                            ],
                          ),
                          Text(chatData[i].time)
                        ],
                      ),
                      const Padding(
                        padding: EdgeInsets.only(
                          left: 60,
                          right: 10,
                        ),
                        child: Divider(
                          thickness: 1.50,
                        ),
                      ),
                    ],
                  ),
                );
              },
            ),
          ),
        ],
      ),
      floatingActionButton: FloatingActionButton(
        backgroundColor: primaryColor,
        onPressed: () {},
        child: const Icon(Icons.chat),
      ),
    );
  }
}
