import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/models/status_model.dart';
import 'package:whatsapp_clone/presentation/widgets/theme/style.dart';

class StatusPage extends StatelessWidget {
  const StatusPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        floatingActionButton: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            SizedBox(
              height: 60,
              width: 45,
              child: FloatingActionButton(
                backgroundColor: Colors.grey[200],
                child: const Icon(Icons.edit, color: Colors.blueGrey,),
                onPressed: () {},
              ),
            ),
            SizedBox(
              height: 70,
              width: 60,
              child: FloatingActionButton(
                backgroundColor: primaryColor,
                child: const Icon(Icons.camera_alt, color: Colors.white,),
                onPressed: () {},
              ),
            ),
          ],
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              _storyWidget(),
              const SizedBox(height: 8),
              _recentTextWidget(),
              const SizedBox(height: 8),
              _listStories()
            ],
          ),
        ));
  }

  Widget _storyWidget() {
    return Container(
      child: Row(
        children: [
          SizedBox(
            height: 55,
            width: 55,
            child: Stack(
              children: [
                Padding(
                  padding: const EdgeInsets.all(2.0),
                  child: Image.asset("assets/profile/profile_default1.jpeg"),
                ),
                Positioned(
                  right: 0,
                  bottom: 0,
                  child: Container(
                    width: 20,
                    height: 20,
                    decoration: const BoxDecoration(
                        color: primaryColor,
                        borderRadius: BorderRadius.all(Radius.circular(20))),
                    child: const Icon(
                      Icons.add,
                      color: Colors.white,
                      size: 15,
                    ),
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(
            width: 12,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: const [
              Text(
                'My Status',
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.w500,
                ),
              ),
              SizedBox(
                height: 2,
              ),
              Text(
                'Tap to add status update',
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget _recentTextWidget() {
    return Container(
      width: double.infinity,
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 4,
      ),
      decoration: BoxDecoration(color: Colors.grey[200]),
      child: const Text('Recent updates'),
    );
  }

  // bottom list
  Widget _listStories() {
    return ListView.builder(
        itemCount: statusData.length,
        shrinkWrap: true,
        physics: const ScrollPhysics(),
        itemBuilder: (context, i) {
          return Container(
            margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        //for image
                        SizedBox(
                          height: 55,
                          width: 55,
                          child: ClipRRect(
                            borderRadius:
                                const BorderRadius.all(Radius.circular(25)),
                            child: Image.asset(statusData[i].avatar),
                          ),
                        ),
                        const SizedBox(
                          width: 10,
                        ),
                        // chats
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              statusData[i].name,
                              style: const TextStyle(
                                fontSize: 18,
                                fontWeight: FontWeight.w500,
                              ),
                            ),
                            const SizedBox(
                              height: 5,
                            ),
                            Text(
                              statusData[i].time,
                              maxLines: 1,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ],
                        )
                      ],
                    ),
                  ],
                ),
                const Padding(
                  padding: EdgeInsets.only(
                    left: 60,
                    right: 10,
                  ),
                  child: Divider(
                    thickness: 1.50,
                  ),
                ),
              ],
            ),
          );
        });
  }
}
