import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/widgets/custom_tab_bar.dart';
import 'package:whatsapp_clone/presentation/widgets/tab_bar_view_widget.dart';
import 'package:whatsapp_clone/presentation/widgets/theme/style.dart';
import '../pages/calls_page.dart';
import '../pages/camera_page.dart';
import '../pages/chat_page.dart';
import '../pages/status_page.dart';
import '../widgets/tab_bar.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isSearch = false;
  final int _currentPageIndex = 1;
  final PageController _pageViewController = PageController(initialPage: 1);
  // final List _pages = const [
  //   CameraPage(),
  //   ChatPage(),
  //   StatusPage(),
  //   CallsPage(),
  // ];

  _buildSearch() {
    return Container(
      height: 45,
      margin: const EdgeInsets.only(top: 25),
      decoration: BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.black.withOpacity(.3),
              spreadRadius: 1,
              offset: const Offset(0.0, 0.50)),
        ],
      ),
      child: TextField(
        decoration: InputDecoration(
          hintText: "Search...",
          prefixIcon: InkWell(
            onTap: () {
              // TODO:
              setState(() {
                _isSearch = false;
              });
            },
            child: const Icon(Icons.arrow_back),
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
          appBar: _currentPageIndex != 0 ? AppBar(
            elevation: 0.0,
            backgroundColor: _isSearch == false ? primaryColor : Colors
                .transparent,
            automaticallyImplyLeading: false,
            bottom: const PreferredSize(preferredSize: Size.fromHeight(50),
              child: TabBarWidget(),),
            title: _isSearch == false
                ? const Text("WhatsApp Clone")
                : const SizedBox(
              height: 0.0,
              width: 0.0,
            ),
            flexibleSpace: _isSearch == false
                ? const Text(
              "",
              style: TextStyle(fontSize: 0.0),
            )
                : _buildSearch(),
            actions: [
              InkWell(
                onTap: () {
                  setState(() {
                    _isSearch = true;
                  });
                },
                child: const Icon(Icons.search),
              ),
              const SizedBox(
                width: 5,
              ),
              const Icon(Icons.more_vert),
            ],
          ) : null,
        body: const TabBarViewWidget(),
      ),
    );
  }
}

// Column(
// children: [
// //  TODO: CustomTabBar
// _isSearch == false
// ? _currentPageIndex != 0 ? CustomTabBar(index: _currentPageIndex)
// : const SizedBox(
// height: 0.0,
// width: 0.0,
// ): const SizedBox(
// height: 0.0,
// width: 0.0,
// ),
// Expanded(
// child: PageView.builder(
// itemCount: _pages.length,
// controller: _pageViewController,
// onPageChanged: (index) {
// setState(() {
// _currentPageIndex = index;
// });
// },
// itemBuilder: (_, index) {
// return _pages[index];
// },
// ),
// ),
// ],
// ),