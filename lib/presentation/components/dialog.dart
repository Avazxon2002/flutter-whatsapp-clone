import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/models/call_model.dart';

class DialogComponent extends StatelessWidget {
  const DialogComponent({Key? key}) : super(key: key);


  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: const Text('User Name'),
      content: const Text("Description"),
      actions: [
        TextButton(
          onPressed: () => Navigator.pop(context, 'Cancel'),
          child: const Text('Cancel'),
        ),
        TextButton(
          onPressed: () => Navigator.pop(context, 'OK'),
          child: const Text('OK'),
        ),
      ],
    );
  }
}
