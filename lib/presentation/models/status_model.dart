class StatusModel {
  final String name;
  final String time;
  final String avatar;

  StatusModel({required this.name, required this.time, required this.avatar});
}

List<StatusModel> statusData = [
  StatusModel(
    name: "Ahror",
    time: "10:20",
    avatar: "assets/profile/profile_default12.jpeg",
  ),
  StatusModel(
    name: "Inom",
    time: "14:23",
    avatar: "assets/profile/profile_default5.jpeg",
  ),
  StatusModel(
    name: "Ibrohim",
    time: "23:20",
    avatar: "assets/profile/profile_default7.jpeg",
  ),
  StatusModel(
    name: "Husan",
    time: "22:30",
    avatar: "assets/profile/profile_default10.jpeg",
  ),
];