import 'package:flutter/material.dart';
import 'package:whatsapp_clone/presentation/widgets/theme/style.dart';

class CallModel {
  final String name;
  final String time;
  final String avatar;
  final Icon callType;

  CallModel({required this.name, required this.time, required this.avatar, required this.callType});
  static Icon callReceived = const Icon(
    Icons.call_received,
    size: 18,
    color: primaryColor,
  );
  static Icon callMissed = const Icon(
    Icons.call_missed,
    size: 18,
    color: primaryColor,
  );
}

List<CallModel> callData = [
  CallModel(
    name: "Ahror",
    time: "10:20",
    callType: CallModel.callReceived,
    avatar: "assets/profile/profile_default1.jpeg",
  ),
  CallModel(
    name: "Rustam",
    time: "14:23",
    callType: CallModel.callMissed,
    avatar: "assets/profile/profile_default3.jpeg",
  ),
  CallModel(
    name: "Inom",
    time: "23:20",
    callType: CallModel.callReceived,
    avatar: "assets/profile/profile_default5.jpeg",
  ),
  CallModel(
    name: "Timur",
    time: "02:00",
    callType: CallModel.callMissed,
    avatar: "assets/profile/profile_default8.jpeg",
  ),
];