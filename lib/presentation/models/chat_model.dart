class ChatModel {
  final String name;
  final String message;
  final String time;
  final String avatar;

  ChatModel(
      {required this.name,
      required this.message,
      required this.time,
      required this.avatar});
}

List<ChatModel> chatData = [
  ChatModel(
    name: "Ahror",
    message: "Assalomu alaykum",
    time: "10:20",
    avatar: "assets/profile/profile_default1.jpeg",
  ),
  ChatModel(
    name: "Rashid",
    message: "Hello",
    time: "22:30",
    avatar: "assets/profile/profile_default2.jpeg",
  ),
  ChatModel(
    name: "Rustam",
    message: "Good",
    time: "14:23",
    avatar: "assets/profile/profile_default3.jpeg",
  ),
  ChatModel(
    name: "Ali",
    message: "Cool",
    time: "11:45",
    avatar: "assets/profile/profile_default4.jpeg",
  ),
  ChatModel(
    name: "Inom",
    message: "Uydaman",
    time: "23:20",
    avatar: "assets/profile/profile_default5.jpeg",
  ),
  ChatModel(
    name: "Masrur",
    message: "Hop",
    time: "2:20",
    avatar: 'assets/profile/profile_default6.jpeg',
  ),
  ChatModel(
    name: "Ibrohim",
    message: "Tel",
    time: "21:20",
    avatar: "assets/profile/profile_default7.jpeg",
  ),
  ChatModel(
    name: "Timur",
    message: "ok",
    time: "18:36",
    avatar: 'assets/profile/profile_default8.jpeg',
  ),
  ChatModel(
    name: "Shohrux",
    message: "hop",
    time: "01:20",
    avatar: 'assets/profile/profile_default9.jpeg',
  ),
  ChatModel(
    name: "Husan",
    message: "Aytdim",
    time: "09:02",
    avatar: 'assets/profile/profile_default10.jpeg',
  ),
  ChatModel(
    name: "Hasan",
    message: "Aytib qo'yin",
    time: "19:08",
    avatar: 'assets/profile/profile_default11.jpeg',
  ),
  ChatModel(
    name: "John",
    message: "me too",
    time: "03:00",
    avatar: 'assets/profile/profile_default12.jpeg',
  ),
];
